import { useState } from 'react';
import Button from "./components/Button/Button";
import ModalImg from './components/Modal/ModalImg';
import ModalText from './components/Modal/ModalText';
import "./index.scss"


function App() {


  const [openModal1, setOpenModal1] = useState(false);
  const [openModal2, setOpenModal2] = useState(false);


  const handleClick1 = () => {
    setOpenModal1(true)
  };
  const handleClick2 = () => {
    setOpenModal2(true)
  };






  return (


    <>
      {openModal1 ? <ModalImg openModal1={openModal1} setOpenModal1={setOpenModal1} /> : null}
      {openModal2 ? <ModalText openModal2={openModal2} setOpenModal2={setOpenModal2} /> : null}

      <Button type="button" classNames="btn button button-primary" onClick={handleClick1}>
        Open first modal</Button>
      <Button type="button" classNames="btn button button-primary" onClick={handleClick2}>
        Open second modal</Button>
    </>

  )
}

export default App
