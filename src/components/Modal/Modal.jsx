
import './Modal.scss'



function Modal({ children, onClick }) {

    return (
        <div className="modal" onClick={onClick}>
            {children}

        </div>
    );
}

export default Modal