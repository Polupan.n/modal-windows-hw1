import Button from "../../Button/Button";
import './ModalHeader.scss'


function ModalHeader({ children }) {

    return (
        <div className="modal-header">
            {children}
        </div>
    )

}

export default ModalHeader;