import ModalBody from './ModalBody';
import ModalContent from './ModalContent';
import ModalFooter from './ModalFooter';
import ModalHeader from './ModalHeader';
import './ModalWrapper.scss'


function ModalWrapper({ children }) {

    return (
        <div className='modal-wrapper'>
            {children}
        </div>

    );

}

export default ModalWrapper

