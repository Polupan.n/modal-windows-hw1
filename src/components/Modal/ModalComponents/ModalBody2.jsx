import './ModalBody.scss';

function ModalBody({ children }) {


    return (
        <div className='modal-body'>
            <h1 className='firstText'>Add Product “NAME”</h1>
            <p className='secondaryText'>Description for you product</p>
            {children}
        </div>
    );
}

export default ModalBody;
