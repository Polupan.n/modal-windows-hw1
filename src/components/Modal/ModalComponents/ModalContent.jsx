import ModalBody1 from "./ModalBody";
// import ModalBody2 from "./ModalBody2";
import ModalFooter from "./ModalFooter";
import ModalHeader from "./ModalHeader";

import './ModalContent.scss'


function ModalContent({ children }) {

    return (

        <div className="modal-content" >
            {children}
        </div>

    );

}

export default ModalContent