import Modal from "./Modal"
import ModalContent from "./ModalComponents/ModalContent";
import ModalWrapper from "./ModalComponents/ModalWrapper";
import ModalHeader from "./ModalComponents/ModalHeader";
import ModalClose from "./ModalComponents/ModalClose";
import ModalBody from "./ModalComponents/ModalBody";
import ModalFooter from "./ModalComponents/ModalFooter";
import image from "../../img/nature.jpeg"
import Button from "../Button/Button";
import '../../img/img.scss'



function ModalImg({ openModal1, setOpenModal1 }) {


    return openModal1 ? (

        <Modal onClick={(event) => {
            if (event.target.classList.contains('modal-wrapper')) {
                setOpenModal1(!openModal1)
            }
        }
        }>
            <ModalWrapper>
                <ModalContent>
                    <ModalHeader>
                        <ModalClose onClick={() => { setOpenModal1(!openModal1) }} />
                    </ModalHeader>
                    <ModalBody>
                        <img src={image} alt="nature" className='modal-img' />
                        <h1 className='firstText'>Product Delete!</h1>
                        <p className='secondaryText'>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button classNames='btn button-primary'>NO, CANCEL</Button>
                        <Button classNames='btn button-secondary'>YES, DELETE</Button>
                    </ModalFooter>
                </ModalContent>
            </ModalWrapper>
        </Modal>

    ) : null



};

export default ModalImg;