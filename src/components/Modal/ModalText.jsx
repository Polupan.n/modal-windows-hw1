import Modal from "./Modal"
import ModalContent from "./ModalComponents/ModalContent";
import ModalWrapper from "./ModalComponents/ModalWrapper";
import ModalHeader from "./ModalComponents/ModalHeader";
import ModalClose from "./ModalComponents/ModalClose";
import ModalBody from "./ModalComponents/ModalBody";
import ModalFooter from "./ModalComponents/ModalFooter";
import Button from "../Button/Button";



function ModalText({ openModal2, setOpenModal2 }) {


    return openModal2 ? (


        <Modal onClick={(event) => {
            if (event.target.classList.contains('modal-wrapper')) {
                setOpenModal2(!openModal2)
            }
        }
        }>

            <ModalWrapper>
                <ModalContent>
                    <ModalHeader>
                        <ModalClose onClick={() => { setOpenModal2(!openModal2) }} />
                    </ModalHeader>
                    <ModalBody>
                        <h1 className='firstText'>Add Product “NAME”</h1>
                        <p className='secondaryText'>Description for you product</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button classNames='btn button-primary'>ADD TO FAVORITE</Button>
                    </ModalFooter>
                </ModalContent>
            </ModalWrapper>
        </Modal>
    ) : null

};

export default ModalText;